from google.cloud import storage, bigquery
from datetime import datetime, timedelta
import os

# Set up Google Cloud Storage Client
client = storage.Client()

# Define your bucket name
bucket_name = 'tango-vm-prod-messenger'

# Get the bucket
bucket = client.get_bucket(bucket_name)

# Calculate the date 30 days ago
date_30_days_ago = datetime.now() - timedelta(days=30)

# List objects in the bucket modified in the last 30 days
modified_objects = []
for blob in bucket.list_blobs():
    if blob.updated > date_30_days_ago:
        modified_objects.append({
            "name": blob.name,
            "size": blob.size,
            "last_modified": blob.updated,
            "storage_class": blob.storage_class
        })

# Set up BigQuery Client
bigquery_client = bigquery.Client()

# Define your BigQuery dataset and table
dataset_id = 'tangome-production.Finops'
table_id = 'tango-vm-prod-messenger'

# Prepare the data for BigQuery
rows_to_insert = [
    {
        "name": obj['name'],
        "size": obj['size'],
        "last_modified": obj['last_modified'].strftime("%Y-%m-%d %H:%M:%S"),
        "storage_class": obj['storage_class']
    } for obj in modified_objects
]

# Insert data into BigQuery
table_ref = bigquery_client.dataset(dataset_id).table(table_id)
errors = bigquery_client.insert_rows_json(table_ref, rows_to_insert)

# Check for errors in inserting data
if errors:
    print("Encountered errors while inserting rows: {}".format(errors))
else:
    print("Rows have been successfully inserted.")
